import ownerAPI from "../api";

export const getAllOwners = () => async (dispatch, getState) => {
  const { page, limit } = getState().paginate;
  const sort = getState().sort;
  const search = { ...getState().search };
  Object.entries(search).forEach(([key, value]) => {
    if (value === "" || value === null) {
      delete search[key];
    }
  });
  try {
    const { data } = await ownerAPI.get("/owners", {
      params: {
        ...search,
        page,
        limit,
        sort: sort,
      },
    });
    data.data.owners.forEach((owner) => {
      owner.isSelected = false;
    });

    dispatch({
      type: "get-all-owners",
      payload: data,
    });
    dispatch(setPagination(data.pagination));
  } catch (err) {
    if (err) {
      if (err.message === "Network Error") {
        dispatch(
          setErrMessage("Internet Connection error or server is offline")
        );
      } else {
        dispatch(setErrMessage(err.response.data.message));
      }
    }
  }
};

export const deleteOwner = (id) => async (dispatch) => {
  try {
    const { data } = await ownerAPI.delete(`/owners/${id}`);
    dispatch(setSucMessage(data.message));
    dispatch(getAllOwners());
  } catch (err) {
    dispatch(setErrMessage(err.response.data.message));
  }
};

export const setErrMessage = (message) => {
  return {
    type: "set-error-message",
    payload: message,
  };
};

export const setSucMessage = (message) => {
  return {
    type: "set-success-message",
    payload: message,
  };
};

export const setLimit = (limit) => {
  return {
    type: "set-limit",
    payload: limit,
  };
};

export const setPage = (page) => {
  return {
    type: "set-page",
    payload: page,
  };
};

export const setPagination = (pagination) => {
  return {
    type: "set-pagination",
    payload: pagination,
  };
};

export const handelSelected = (owners) => {
  return {
    type: "handel-selected",
    payload: owners,
  };
};
export const deleteMultiple = (ids) => async (dispatch, getState) => {
  try {
    const { data } = await ownerAPI.delete(`/owners/${ids.join()}`);
    dispatch(setSucMessage(data.message));
    dispatch(getAllOwners());
  } catch (err) {
    dispatch(setErrMessage("Something went wrong"));
  }
};

export const handelSorting = (sort) => {
  return {
    type: "handel-sort",
    payload: sort,
  };
};

export const searchName = (data) => {
  return {
    type: "search-name",
    payload: data,
  };
};
export const searchEmail = (data) => {
  return {
    type: "search-email",
    payload: data,
  };
};
export const searchMobileNumber = (data) => {
  return {
    type: "search-mobile-number",
    payload: data,
  };
};
export const searchGender = (data) => {
  return {
    type: "search-gender",
    payload: data,
  };
};
export const searchBio = (data) => {
  return {
    type: "search-bio",
    payload: data,
  };
};
export const searchRole = (data) => {
  return {
    type: "search-role",
    payload: data,
  };
};
export const searchActiveStatus = (data) => {
  return {
    type: "search-active-status",
    payload: data,
  };
};
