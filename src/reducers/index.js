import { combineReducers } from "redux";

const ownerReducer = (ownerData = { owners: [], totalRecords: 0 }, action) => {
  switch (action.type) {
    case "get-all-owners":
      return {
        ...ownerData,
        owners: [...action.payload.data.owners],
        // owners: [...ownerData.owners, ...action.payload.data.owners],
        totalRecords: action.payload.totalRecords,
      };
    case "handel-selected":
      return {
        ...ownerData,
        owners: [...action.payload],
      };

    default:
      return ownerData;
  }
};

const paginateReducer = (
  paginate = { limit: 5, page: 1, pagination: {} },
  action
) => {
  switch (action.type) {
    case "set-limit":
      return { ...paginate, limit: action.payload, page: 1 };
    case "set-page":
      return { ...paginate, page: action.payload };
    case "set-pagination":
      return { ...paginate, pagination: action.payload };
    default:
      return paginate;
  }
};

const messageReducer = (
  message = { successMessage: "", errMessage: "" },
  action
) => {
  switch (action.type) {
    case "set-success-message":
      return { ...message, successMessage: action.payload, errMessage: "" };
    case "set-error-message":
      return { ...message, errMessage: action.payload, successMessage: "" };
    default:
      return message;
  }
};

const sortingReducer = (sort = "created_at", action) => {
  switch (action.type) {
    case "handel-sort":
      return action.payload;
    default:
      return sort;
  }
};

const searchReducer = (
  search = {
    name: "",
    email: "",
    mobile_number: "",
    gender: "",
    bio: "",
    role: "",
    active_status: "",
  },
  action
) => {
  switch (action.type) {
    case "search-name":
      return { ...search, name: action.payload };
    case "search-email":
      return { ...search, email: action.payload };
    case "search-mobile-number":
      return { ...search, mobile_number: action.payload };
    case "search-gender":
      return { ...search, gender: action.payload };
    case "search-bio":
      return { ...search, bio: action.payload };
    case "search-role":
      return { ...search, role: action.payload };
    case "search-active-status":
      return { ...search, active_status: action.payload };
    default:
      return search;
  }
};

export default combineReducers({
  ownerData: ownerReducer,
  paginate: paginateReducer,
  message: messageReducer,
  sort: sortingReducer,
  search: searchReducer,
});
