import "bootstrap/dist/css/bootstrap.css";
import "bootstrap/dist/js/bootstrap";
import "./App.css";
import React from "react";
import Navbar from "./Components/Navbar";
import Home from "./Components/Home";

function App() {
  return (
    <div className="App">
      <Navbar />
      <div className="px-5 mt-5 pt-3">
        <Home />
      </div>
    </div>
  );
}

export default App;
