import React, { useState } from "react";
import { useFormik } from "formik";
import ownerAPI from "../../api";
import { useDispatch } from "react-redux";
import { setErrMessage, setSucMessage, getAllOwners } from "../../actions";
import $ from "jquery";
import _ from "lodash";

const Form = ({ title, buttonName, modalId, formRole, initialValues }) => {
  const validate = (values) => {
    const errors = {};
    // name
    if (!values.name) {
      errors.name = "Owner's name must be required";
    }
    // email
    if (!values.email) {
      errors.email = "Owner's email must be required";
    } else if (
      !/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(
        values.email
      )
    ) {
      errors.email = "Invalid email format";
    }
    // mobile number
    if (!values.mobile_number) {
      errors.mobile_number = "Owner's phone number must be required";
    } else if (
      values.mobile_number.length < 10 ||
      values.mobile_number.length > 10 ||
      !RegExp(/^[0-9]{10}$/g).test(values.mobile_number)
    ) {
      errors.mobile_number = "Invalid phone number";
    }
    // gender
    if (!values.gender) {
      errors.gender = "Owner's gender must be required";
    }

    // hobbies
    if (values.hobbies.length <= 0) {
      errors.hobbies = "Atleast one hobbie of owner is required";
    }

    return errors;
  };

  const dispacth = useDispatch();

  const [photo, setPhoto] = useState(null);
  const [isLoading, setIsLoading] = useState(false);

  const hideModal = () => {
    $(`#${modalId}`).modal("hide");
  };

  const compareValues = () => {
    return _.isEqual(initialValues, formik.values);
  };

  const onSubmit = (values) => {
    const data = new FormData();
    data.append("name", values.name);
    data.append("email", values.email);
    data.append("mobile_number", values.mobile_number);
    data.append("bio", values.bio);
    data.append("role", values.role);
    data.append("gender", values.gender);
    values.hobbies.forEach((hobbie) => {
      data.append("hobbies", parseInt(hobbie));
    });
    data.append("active_status", values.active_status ? 1 : 0);
    if (formRole !== "update") {
      if (photo) {
        data.append("photo", photo);
      }
    }
    setIsLoading(true);

    if (formRole !== "update") {
      ownerAPI
        .post("/owners", data)
        .then((res) => {
          setIsLoading(false);
          formik.resetForm();
          dispacth(getAllOwners());
          dispacth(setSucMessage(`owner created ${res.data.data.ownerId}`));
          hideModal();
        })
        .catch(({ response }) => {
          setIsLoading(false);
          dispacth(setErrMessage(response.data.message));
          hideModal();
        });
    } else {
      ownerAPI
        .patch(`/owners/${initialValues.id}`, data)
        .then((res) => {
          setIsLoading(false);
          dispacth(getAllOwners());
          dispacth(setSucMessage(res.data.message));
          hideModal();
        })
        .catch(({ response }) => {
          setIsLoading(false);
          dispacth(setErrMessage(response.data.message));
          hideModal();
        });
    }
  };

  const formik = useFormik({
    initialValues,
    validate,
    onSubmit,
  });

  return (
    <div
      className="modal fade"
      id={modalId}
      tabIndex="-1"
      role="dialog"
      aria-hidden="true"
    >
      <div className="modal-dialog custom-modal-dialog" role="document">
        <div className="modal-content bg-dark text-light">
          <div className="modal-header">
            <h5 className="modal-title">{title}</h5>
            <button
              type="button"
              className="close"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span
                aria-hidden="true"
                className="text-light"
                onClick={() => {
                  if (formRole !== "update") formik.resetForm(initialValues);
                }}
              >
                &times;
              </span>
            </button>
          </div>
          <div className="modal-body">
            {/* Here */}
            <form
              autoComplete="off"
              onSubmit={formik.handleSubmit}
              onReset={formik.handleReset}
            >
              {/* Name */}
              <div className="form-group">
                <input
                  type="text"
                  className="form-control bg-dark text-light "
                  placeholder="Enter Owner Name"
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.name}
                  name="name"
                />
                {formik.errors.name && formik.touched.name ? (
                  <div className="text-danger">{formik.errors.name}</div>
                ) : null}
              </div>
              {/* email */}
              <div className="form-group">
                <input
                  type="email"
                  className="form-control bg-dark text-light "
                  placeholder="Enter Owner Email Address"
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.email}
                  name="email"
                />
                {formik.errors.email && formik.touched.email ? (
                  <div className="text-danger">{formik.errors.email}</div>
                ) : null}
              </div>
              {/* mobile_number */}
              <div className="form-group">
                <input
                  type="text"
                  className="form-control bg-dark text-light "
                  placeholder="Enter Owner Phone Number"
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.mobile_number}
                  name="mobile_number"
                  maxLength={10}
                />
                {formik.errors.mobile_number && formik.touched.mobile_number ? (
                  <div className="text-danger">
                    {formik.errors.mobile_number}
                  </div>
                ) : null}
              </div>
              {/* bio */}
              <div className="form-group">
                <textarea
                  className="form-control bg-dark text-light"
                  rows="4"
                  placeholder="Enter Owner's Biodata"
                  name="bio"
                  onChange={formik.handleChange}
                  value={formik.values.bio}
                  style={{ resize: "none" }}
                ></textarea>
              </div>
              {/* active status */}
              <div className="form-group pl-4">
                <label className="form-check-label">
                  <input
                    className="form-check-input"
                    type="checkbox"
                    name="active_status"
                    onChange={formik.handleChange}
                    checked={formik.values.active_status}
                    value={formik.values.active_status}
                  />
                  Active Status
                </label>
              </div>
              {/* gender */}
              <div className="form-group">
                <div className="form-check form-check-inline">
                  <input
                    className="form-check-input"
                    type="radio"
                    name="gender"
                    value="male"
                    checked={formik.values.gender === "male"}
                    onBlur={formik.handleBlur}
                    onChange={formik.handleChange}
                  />
                  <label className="form-check-label">Male</label>
                </div>
                <div className="form-check form-check-inline">
                  <input
                    className="form-check-input"
                    type="radio"
                    name="gender"
                    value="female"
                    checked={formik.values.gender === "female"}
                    onBlur={formik.handleBlur}
                    onChange={formik.handleChange}
                  />
                  <label className="form-check-label">Female</label>
                </div>
                <div className="form-check form-check-inline">
                  <input
                    className="form-check-input"
                    type="radio"
                    name="gender"
                    value="other"
                    checked={formik.values.gender === "other"}
                    onBlur={formik.handleBlur}
                    onChange={formik.handleChange}
                  />
                  <label className="form-check-label">Other</label>
                </div>
                {formik.errors.gender && formik.touched.gender ? (
                  <div className="text-danger">{formik.errors.gender}</div>
                ) : null}
              </div>
              {/* role */}
              <div className="form-group">
                <div className="d-flex align-items-center mt-3">
                  <p className="m-0 mr-2">Role</p>
                  <select
                    className="form-control form-control-sm w-25 bg-dark text-light"
                    name="role"
                    onChange={formik.handleChange}
                    value={formik.values.role}
                  >
                    <option value="student">Student</option>
                    <option value="teacher">Teacher</option>
                    <option value="contractor">Contractor</option>
                  </select>
                </div>
              </div>
              {/* hobbies */}
              <div className="form-group">
                <div>
                  <p className="mt-3 mb-1">Hobbies</p>
                  <div className="row">
                    <div className="form-check form-check-inline col-3">
                      <input
                        className="form-check-input ml-3"
                        type="checkbox"
                        value={1}
                        name="hobbies"
                        checked={formik.values.hobbies.includes("1")}
                        onChange={formik.handleChange}
                        onClick={formik.handleBlur}
                      />
                      <label className="form-check-label">Reading</label>
                    </div>

                    <div className="form-check form-check-inline col-3">
                      <input
                        className="form-check-input ml-3"
                        type="checkbox"
                        value={2}
                        name="hobbies"
                        checked={formik.values.hobbies.includes("2")}
                        onChange={formik.handleChange}
                        onClick={formik.handleBlur}
                      />
                      <label className="form-check-label">Writting</label>
                    </div>

                    <div className="form-check form-check-inline col-3">
                      <input
                        className="form-check-input ml-3"
                        type="checkbox"
                        value={3}
                        name="hobbies"
                        checked={formik.values.hobbies.includes("3")}
                        onChange={formik.handleChange}
                        onClick={formik.handleBlur}
                      />
                      <label className="form-check-label">Designing</label>
                    </div>

                    <div className="form-check form-check-inline col-3">
                      <input
                        className="form-check-input ml-3"
                        type="checkbox"
                        value={4}
                        name="hobbies"
                        checked={formik.values.hobbies.includes("4")}
                        onChange={formik.handleChange}
                        onClick={formik.handleBlur}
                      />
                      <label className="form-check-label">Swimming</label>
                    </div>

                    <div className="form-check form-check-inline col-3">
                      <input
                        className="form-check-input ml-3"
                        type="checkbox"
                        value={5}
                        name="hobbies"
                        checked={formik.values.hobbies.includes("5")}
                        onChange={formik.handleChange}
                        onClick={formik.handleBlur}
                      />
                      <label className="form-check-label">Developing</label>
                    </div>

                    <div className="form-check form-check-inline col-3">
                      <input
                        className="form-check-input ml-3"
                        type="checkbox"
                        value={6}
                        name="hobbies"
                        checked={formik.values.hobbies.includes("6")}
                        onChange={formik.handleChange}
                        onClick={formik.handleBlur}
                      />
                      <label className="form-check-label">Skydiving</label>
                    </div>
                    {formik.errors.hobbies && formik.touched.hobbies ? (
                      <div className="text-danger ml-3">
                        {formik.errors.hobbies}
                      </div>
                    ) : null}
                  </div>
                </div>
              </div>
              {/* file */}
              {formRole !== "update" ? (
                <div className="form-group mt-3">
                  <label>Upload Image</label>
                  <input
                    type="file"
                    className="form-control-file"
                    onChange={(e) => setPhoto(e.target.files[0])}
                  />
                </div>
              ) : null}
              {/* submit button */}
              <div className="text-right">
                <button
                  type="submit"
                  className="btn btn-outline-primary px-4"
                  disabled={
                    !formik.isValid ||
                    isLoading ||
                    (compareValues() && formRole === "update")
                    // !formik.dirty
                  }
                >
                  {isLoading ? (
                    <>
                      <span
                        className="spinner-border spinner-border-sm"
                        role="status"
                        aria-hidden="true"
                      ></span>
                      <span className="sr-only">Sending...</span>
                    </>
                  ) : (
                    buttonName
                  )}
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Form;
