import React from "react";

const Navbar = () => {
  return (
    <nav className="navbar navbar-dark bg-dark fixed-top">
      <div className="container">
        <div className="d-flex justify-content-between align-items-center w-100">
          <div>
            <span className="navbar-brand mb-0 h1">Owners Menu</span>
          </div>
          <div></div>
        </div>
      </div>
    </nav>
  );
};

export default Navbar;
