import React from "react";

import { useDispatch, useSelector } from "react-redux";
import { deleteMultiple } from "../../actions";

const DeleteConfirmModal = () => {
  const owners = useSelector((state) => state.ownerData.owners);
  const dispatch = useDispatch();
  const ids = [];
  owners.forEach((owner) => {
    if (owner.isSelected) {
      ids.push(owner.owner_id);
    }
  });

  const onButtonClick = () => {
    dispatch(deleteMultiple(ids));
  };

  return (
    <div
      className="modal fade"
      id="deleteMultipleModal"
      tabIndex="-1"
      role="dialog"
      aria-hidden="true"
    >
      <div className="modal-dialog " role="document">
        <div className="modal-content bg-dark text-light">
          <div className="modal-header">
            <h5 className="modal-title">Delete Confirmation</h5>
            <button
              type="button"
              className="close"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true" className="text-light">
                &times;
              </span>
            </button>
          </div>
          <div className="modal-body">
            Are you sure you want to
            <span className="text-danger font-weight-bold"> delete </span>
            owners with ids
            <span className="text-monospace text-danger font-weight-bold">
              {" "}
              {ids.toString()}
            </span>
          </div>
          <div className="modal-footer">
            <button
              type="button"
              className="btn btn-secondary btn-sm"
              data-dismiss="modal"
            >
              Cancel
            </button>
            <button
              type="button"
              className="btn btn-danger btn-sm"
              onClick={onButtonClick}
              data-dismiss="modal"
            >
              Delete
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default DeleteConfirmModal;
