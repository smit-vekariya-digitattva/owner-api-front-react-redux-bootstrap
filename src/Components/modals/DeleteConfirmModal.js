import React from "react";
import { deleteOwner } from "../../actions";
import { useDispatch } from "react-redux";
function DeleteConfirmModal({ owner_id }) {
  const dispatch = useDispatch();
  return (
    <div
      className="modal fade"
      id={`delete-owner-${owner_id}`}
      tabIndex="-1"
      role="dialog"
      aria-hidden="true"
    >
      <div className="modal-dialog " role="document">
        <div className="modal-content bg-dark text-light">
          <div className="modal-header">
            <h5 className="modal-title">Delete Confirmation</h5>
            <button
              type="button"
              className="close"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span className="text-light" aria-hidden="true">
                &times;
              </span>
            </button>
          </div>
          <div className="modal-body">
            Are you sure you want to
            <span className="text-danger font-weight-bold"> delete </span>
            owner with id
            <span className="text-monospace text-danger font-weight-bold">
              {" "}
              {owner_id}
            </span>
          </div>
          <div className="modal-footer">
            <button
              type="button"
              className="btn btn-secondary btn-sm"
              data-dismiss="modal"
            >
              Cancel
            </button>
            <button
              type="button"
              className="btn btn-danger btn-sm"
              data-dismiss="modal"
              onClick={() => dispatch(deleteOwner(owner_id))}
            >
              Delete
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default DeleteConfirmModal;
