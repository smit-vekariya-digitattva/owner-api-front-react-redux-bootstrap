import React, { useState } from "react";
import ownerAPI from "../../api";
import { useDispatch } from "react-redux";
import { setErrMessage, getAllOwners, setSucMessage } from "../../actions";
import $ from "jquery";
const ImageUpdateModal = ({ imageModalId, imageName, ownerName, id }) => {
  const [photo, setPhoto] = useState(null);
  const [err, setErr] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const dispatch = useDispatch();

  const hideModal = () => {
    $(`#${imageModalId}`).modal("hide");
  };

  const onButtonClick = (e) => {
    if (!photo) {
      setErr("Please select a file first");
    }
    if (photo) {
      const data = new FormData();
      data.append("photo", photo);
      setIsLoading(true);
      ownerAPI
        .patch(`/owners/${id}/update/image`, data)
        .then((res) => {
          dispatch(setSucMessage(res.data.message));
          setIsLoading(false);
          setErr("");
          setPhoto(null);
          dispatch(getAllOwners());
          hideModal();
        })
        .catch(({ response }) => {
          setIsLoading(false);
          setErr("");
          dispatch(setErrMessage(response.data.message));
          hideModal();
        });
    }
    e.preventDefault();
  };

  return (
    <div
      className="modal fade"
      id={imageModalId}
      tabIndex="-1"
      role="dialog"
      aria-hidden="true"
    >
      <div className="modal-dialog" role="document">
        <div className="modal-content bg-dark text-light">
          <div className="modal-header">
            <h5 className="modal-title">Update Image</h5>
            <button
              type="button"
              className="close"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true" className="text-light">
                &times;
              </span>
            </button>
          </div>
          <div className="modal-body">
            <div className="card bg-dark text-light">
              <img
                className="card-img-top custom-img mt-2"
                src={`http://192.168.0.102:3001/images/${imageName}`}
                alt="Card cap"
              />
              <div className="card-body">
                <h5 className="card-title">{ownerName}</h5>
                <form className="mt-4">
                  <div className="form-group mt-3">
                    <input
                      type="file"
                      className="form-control-file bg-dark"
                      onChange={(e) => {
                        setPhoto(e.target.files[0]);
                        if (e.target.files[0]) {
                          setErr("");
                        }
                      }}
                    />
                    {err ? <div className="text-danger">{err}</div> : null}
                  </div>
                  <div className="text-right">
                    <button
                      className="btn btn-outline-primary  px-4"
                      onClick={onButtonClick}
                      disabled={isLoading}
                    >
                      {isLoading ? (
                        <>
                          <span
                            className="spinner-border spinner-border-sm"
                            role="status"
                            aria-hidden="true"
                          ></span>
                          <span className="sr-only">Sending...</span>
                        </>
                      ) : (
                        "Update Image"
                      )}
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ImageUpdateModal;
