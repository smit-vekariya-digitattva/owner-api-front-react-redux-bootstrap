import React from "react";
import { GoTriangleUp, GoTriangleDown } from "react-icons/go";
import { useDispatch } from "react-redux";
import { handelSorting } from "../actions";

const ArrowButton = ({ data }) => {
  const dispatch = useDispatch();
  const onUpArrowClick = () => {
    dispatch(handelSorting(data));
  };
  const onDownArrowClick = () => {
    dispatch(handelSorting(`${data} desc`));
  };

  return (
    <div className="d-flex flex-column">
      <GoTriangleUp className="cursor" onClick={onUpArrowClick} />
      <GoTriangleDown className="cursor" onClick={onDownArrowClick} />
    </div>
  );
};

export default ArrowButton;
