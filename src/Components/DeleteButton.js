import React from "react";
import { FaTrash } from "react-icons/fa";
import DeleteMultipleConfirmModal from "./modals/DeleteMultipleConfirmModal";

const DeleteButton = () => {
  return (
    <>
      <button
        type="button"
        className="btn btn-outline-danger btn-sm m-1"
        data-toggle="modal"
        data-target="#deleteMultipleModal"
      >
        <FaTrash />
      </button>
      <DeleteMultipleConfirmModal />
    </>
  );
};

export default DeleteButton;
