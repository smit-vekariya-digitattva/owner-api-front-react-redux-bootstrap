import React from "react";
import { FaTrash, FaPencilAlt } from "react-icons/fa";
import Form from "./form/Form";
import ImageUpdateModal from "./modals/ImageUpdateModal";
import DeleteConfirmModal from "./modals/DeleteConfirmModal";

const OwnerRow = ({ owner, onChange }) => {
  const initialValues = {
    id: owner.owner_id,
    name: owner.name,
    email: owner.email,
    mobile_number: owner.mobile_number,
    bio: owner.bio,
    active_status: owner.active_status === 1 ? true : false,
    gender: owner.gender,
    role: owner.role,
    hobbies: owner.hobbies.split(",").map((hobbie) => {
      if (hobbie === "reading") return "1";
      if (hobbie === "writting") return "2";
      if (hobbie === "designing") return "3";
      if (hobbie === "swimming") return "4";
      if (hobbie === "developing") return "5";
      if (hobbie === "skydiving") return "6";
      return null;
    }),
  };

  return (
    <tr>
      <td>
        <input
          type="checkbox"
          checked={owner.isSelected}
          value={owner.owner_id}
          onChange={onChange}
        />
      </td>
      <td>
        <img
          src={`http://192.168.0.102:3001/images/${owner.photo}`}
          alt="owner profile img"
          data-toggle="modal"
          data-target={`#image-update-${owner.owner_id}`}
          className="cursor"
        />
        <ImageUpdateModal
          imageModalId={`image-update-${owner.owner_id}`}
          imageName={owner.photo}
          ownerName={owner.name}
          id={owner.owner_id}
        />
      </td>
      <td>{owner.name}</td>
      <td>{owner.email}</td>
      <td>{owner.mobile_number}</td>
      <td>{owner.gender}</td>
      <td>{owner.bio}</td>
      <td>{owner.role}</td>
      <td>{owner.active_status}</td>
      <td>{owner.hobbies}</td>
      <td>
        <button
          className="btn btn-outline-primary btn-sm m-1"
          data-toggle="modal"
          data-target={`#update-owner-${owner.owner_id}`}
        >
          <FaPencilAlt />
        </button>
        <Form
          title={`Edit Owner ${owner.name}`}
          buttonName="Update Owner"
          modalId={`update-owner-${owner.owner_id}`}
          formRole="update"
          initialValues={initialValues}
        />
        <button
          type="button"
          className="btn btn-outline-danger btn-sm m-1"
          data-toggle="modal"
          data-target={`#delete-owner-${owner.owner_id}`}
        >
          <FaTrash />
        </button>
        <DeleteConfirmModal owner_id={owner.owner_id} />
      </td>
    </tr>
  );
};

export default OwnerRow;
