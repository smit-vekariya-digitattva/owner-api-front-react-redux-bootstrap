import React from "react";
import Table from "./Table";
import PaginationBar from "./PaginationBar";
const Home = () => {
  return (
    <>
      <Table />
      <PaginationBar />
    </>
  );
};

export default Home;
