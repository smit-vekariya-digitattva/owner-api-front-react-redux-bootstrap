import React from "react";
import { FaGreaterThan, FaLessThan } from "react-icons/fa";
import { useDispatch, useSelector } from "react-redux";
import { setLimit, setPage } from "../actions";

function PaginationBar() {
  const dispatch = useDispatch();

  const { limit, page, pagination } = useSelector((state) => state.paginate);
  const { totalRecords } = useSelector((state) => state.ownerData);

  const getPageDataInfo = () => {
    const startIndex = (page - 1) * limit + 1;
    const endIndex = page * limit;
    const total = totalRecords;
    if (limit === 1) {
      return `${startIndex} of ${total}`;
    }
    if (endIndex > total && limit !== 1) {
      return `${startIndex} - ${total}  of ${total}`;
    } else {
      return `${startIndex} - ${endIndex} of ${total}`;
    }
  };

  return (
    <div className="mt-2 row">
      <div className="col-md-6">
        <div className="input-group w-50 ml-md-auto m-2">
          <div className="input-group-prepend">
            <label className="input-group-text" htmlFor="pageLimit">
              Rows Per Page
            </label>
          </div>
          <select
            className="custom-select"
            id="pageLimit"
            value={limit}
            onChange={(e) => dispatch(setLimit(parseInt(e.target.value)))}
          >
            <option value={1}>1</option>
            <option value={5}>5</option>
            <option value={10}>10</option>
            <option value={15}>15</option>
            <option value={20}>20</option>
            <option value={25}>25</option>
            <option value={30}>30</option>
          </select>
        </div>
      </div>
      <div className="col-md-6">
        <div className="px-5 d-flex justify-contect-between align-items-center pt-2 mx-auto">
          <span className="text-monospace">{getPageDataInfo()}</span>
          <div className="px-4">
            <button
              className="custom-button mx-1"
              disabled={pagination.previous ? false : true}
              onClick={() => dispatch(setPage(pagination.previous.page))}
            >
              <FaLessThan />
            </button>
            <button
              className="custom-button mx-1"
              disabled={pagination.next ? false : true}
              onClick={() => dispatch(setPage(pagination.next.page))}
            >
              <FaGreaterThan />
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default PaginationBar;
