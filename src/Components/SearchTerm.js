import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import _ from "lodash";

const SearchTerm = ({ placeholder, onChangeFunc }) => {
  const [searchTerm, setSearchTerm] = useState("");
  const dispatch = useDispatch();

  const delayedQuery = _.debounce((q) => dispatch(onChangeFunc(q)), 500);

  useEffect(() => {
    delayedQuery(searchTerm);

    return () => {
      delayedQuery.cancel();
    };
  }, [searchTerm]);

  return (
    <input
      type="text"
      placeholder={placeholder}
      value={searchTerm}
      onChange={(e) => setSearchTerm(e.target.value)}
      className="form-control"
    />
  );
};
export default SearchTerm;
