import React from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  searchName,
  searchEmail,
  searchBio,
  searchGender,
  searchActiveStatus,
  searchMobileNumber,
  searchRole,
} from "../actions";
import DeleteButton from "./DeleteButton";
import SeacrhTerm from "./SearchTerm";

const SearchRow = () => {
  const search = useSelector((state) => state.search);
  const owners = useSelector((state) => state.ownerData.owners);
  const isDisplay = owners.some((owner) => {
    return owner.isSelected === true;
  });
  const dispatch = useDispatch();

  return (
    <tr style={{ position: "sticky", top: 0 }}>
      <td></td>
      <td></td>
      <td>
        <SeacrhTerm placeholder="search name..." onChangeFunc={searchName} />
      </td>
      <td>
        <SeacrhTerm placeholder="search email..." onChangeFunc={searchEmail} />
      </td>
      <td>
        <SeacrhTerm
          placeholder="search phone no..."
          onChangeFunc={searchMobileNumber}
        />
      </td>
      <td>
        <select
          className="form-control"
          value={search.gender}
          onChange={(e) => dispatch(searchGender(e.target.value))}
        >
          <option value="">All...</option>
          <option value="male">male</option>
          <option value="female">female</option>
          <option value="other">other</option>
        </select>
      </td>
      <td>
        <SeacrhTerm placeholder="search bio..." onChangeFunc={searchBio} />
      </td>
      <td>
        <select
          className="form-control"
          value={search.role}
          onChange={(e) => dispatch(searchRole(e.target.value))}
        >
          <option value="">All...</option>
          <option value="student">student</option>
          <option value="teacher">teacher</option>
          <option value="contractor">contractor</option>
        </select>
      </td>
      <td>
        <select
          className="form-control"
          value={search.active_status}
          onChange={(e) => dispatch(searchActiveStatus(e.target.value))}
        >
          <option value="">All...</option>
          <option value={0}>0</option>
          <option value={1}>1</option>
        </select>
      </td>
      <td></td>
      <td>{isDisplay ? <DeleteButton /> : ""}</td>
    </tr>
  );
};

export default React.memo(SearchRow);
