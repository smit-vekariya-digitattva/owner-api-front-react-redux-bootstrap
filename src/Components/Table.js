import React, { useEffect } from "react";
import TableRows from "./TableRows";
import { useDispatch, useSelector } from "react-redux";
import { getAllOwners, handelSelected, setSucMessage } from "../actions";
import CustomTableHeader from "./CustomTableHeader";
import Form from "./form/Form";

const Table = () => {
  const initialValues = {
    name: "",
    email: "",
    mobile_number: "",
    bio: "",
    active_status: false,
    gender: "",
    role: "student",
    hobbies: [],
  };
  const { limit, page } = useSelector((state) => state.paginate);
  const sort = useSelector((state) => state.sort);
  const { owners } = useSelector((state) => state.ownerData);
  const { successMessage, errMessage } = useSelector((state) => state.message);
  const search = useSelector((state) => state.search);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getAllOwners());
  }, [
    limit,
    page,
    sort,
    search.name,
    search.email,
    search.mobile_number,
    search.bio,
    search.gender,
    search.role,
    search.active_status,
  ]);

  useEffect(() => {
    let id;
    if (document.querySelector(".alert")) {
      id = setTimeout(() => {
        document.querySelector(".alert").style.display = "none";
        dispatch(setSucMessage(""));
      }, 3000);
    }

    return () => {
      clearTimeout(id);
    };
  }, [successMessage, errMessage]);

  const OnCheckBoxChange = (e) => {
    let newOwners = [...owners];
    newOwners.forEach((owner) => {
      owner.isSelected = e.target.checked;
    });
    dispatch(handelSelected(newOwners));
  };

  return (
    <div className="mt-4">
      {errMessage ? (
        <div className="alert alert-danger mb-3" role="alert">
          <strong>Error: </strong>
          {errMessage}
        </div>
      ) : (
        ""
      )}

      {successMessage ? (
        <div className="alert alert-success mb-3" role="alert">
          <strong>Success: </strong>
          {successMessage}
        </div>
      ) : (
        ""
      )}
      <div className="text-right mb-3">
        <button
          className="btn btn-outline-info px-4"
          data-toggle="modal"
          data-target="#add-owner"
        >
          Add Owner
        </button>
      </div>

      <Form
        title="Register Owner"
        buttonName="Add Owner"
        modalId="add-owner"
        formRole="add"
        initialValues={initialValues}
      />
      <div
        className="table-responsive"
        style={{ height: "549px", overflow: "auto" }}
      >
        <table className="table table-hover table-bordered mt-2">
          <thead className="thead-dark" style={{ position: "sticky", top: 0 }}>
            <tr>
              <th scope="col" className="header">
                <input
                  type="checkbox"
                  value="checkall"
                  onChange={OnCheckBoxChange}
                />
              </th>
              <th scope="col" className="header">
                Profile Picture
              </th>
              <CustomTableHeader
                scope="col"
                className="sort"
                label="Full Name"
                data="name"
              />
              <CustomTableHeader
                scope="col"
                className="sort"
                label="Email"
                data="email"
              />
              <CustomTableHeader
                scope="col"
                className="sort"
                label="Mobile Number"
                data="mobile_number"
              />
              <CustomTableHeader
                scope="col"
                className="sort"
                label="Gender"
                data="gender"
              />
              <th scope="col" className="header">
                Bio
              </th>
              <CustomTableHeader
                scope="col"
                className="sort"
                label="Role"
                data="role"
              />
              <CustomTableHeader
                scope="col"
                className="sort"
                label="Active Status"
                data="active_status"
              />

              <th scope="col" className="header">
                Hobby
              </th>
              <th scope="col" className="header">
                Action
              </th>
            </tr>
          </thead>

          <TableRows owners={owners} />
        </table>
      </div>
    </div>
  );
};

export default Table;
