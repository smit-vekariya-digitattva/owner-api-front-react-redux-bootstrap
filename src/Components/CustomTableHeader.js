import React from "react";
import ArrowButton from "./ArrowButton";

const CustomTableHeader = ({ label, data }) => {
  return (
    <th scope="col" className="header">
      <div className="d-flex align-items-center justify-content-start">
        <div className="mr-3">{label}</div>
        <div>
          <ArrowButton data={data} />
        </div>
      </div>
    </th>
  );
};

export default CustomTableHeader;
