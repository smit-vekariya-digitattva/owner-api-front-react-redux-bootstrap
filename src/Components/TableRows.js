import React from "react";
import { useDispatch } from "react-redux";
import { handelSelected } from "../actions";
import SearchRow from "./SearchRow";
import OwnerRow from "./OwnerRow";

const TableRows = ({ owners }) => {
  const dispatch = useDispatch();

  const onCheckBoxChange = (e) => {
    const newOwners = [...owners];
    newOwners.forEach((owner) => {
      if (owner.owner_id === parseInt(e.target.value)) {
        owner.isSelected = e.target.checked;
      }
    });
    dispatch(handelSelected(newOwners));
  };

  return (
    <tbody>
      <SearchRow />

      {owners.length !== 0 ? (
        owners.map((owner) => {
          return (
            <React.Fragment key={owner.owner_id}>
              <OwnerRow owner={owner} onChange={onCheckBoxChange} />
            </React.Fragment>
          );
        })
      ) : (
        <tr>
          <td colSpan={11} className="text-center">
            No Owners Found
          </td>
        </tr>
      )}
    </tbody>
  );
};

export default TableRows;
